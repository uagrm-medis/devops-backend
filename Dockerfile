FROM node:16.17.1-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/scr/app
COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build
RUN npm ci --omit=dev && npm cache clean --force
CMD ["node", "dist/main.js"]
